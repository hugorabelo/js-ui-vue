import { RouteRecordRaw } from 'vue-router'

const CatRoutes: RouteRecordRaw = {
  path: '/dogs',

  component: () => import('layouts/MainLayout.vue'),

  children: [
    {
      path: '',
      component: () => import('pages/dogs/IndexPage.vue'),
      name: 'DogsIndex'
    }
  ]
}

export default CatRoutes
